#!/bin/sh
java -Xms256m -Xmx512m -XX:-TieredCompilation -Xss256k -XX:+UseG1GC -XX:+UseStringDeduplication -Djava.security.egd=file:/dev/./urandom -jar /home/tkn-eureka-server.jar --spring.profiles.active=$PROFILES_ACTIVE
